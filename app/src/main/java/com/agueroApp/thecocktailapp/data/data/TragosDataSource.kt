package com.agueroApp.thecocktailapp.data.data

import com.agueroApp.thecocktailapp.data.data.dataBase.AppDatabase
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.agueroApp.thecocktailapp.vo.Resource
import com.agueroApp.thecocktailapp.vo.RetrofitClient

class TragosDataSource (private val appDatabase: AppDatabase) {

    suspend fun fetchTragosIngredientList(nameIngredient : String): Resource<List<Drink>> {
        return Resource.Success(RetrofitClient.webService.getTragosByIngredients(nameIngredient).drinkList)
    }

    suspend fun fetchTragosAlcoholicList(alcoholic : String): Resource<List<Drink>> {
        return Resource.Success(RetrofitClient.webService.getTragosConAlcohol(alcoholic).drinkList)
    }

    suspend fun fetchTragosNonAlcoholicList(non_alcoholic : String): Resource<List<Drink>> {
        return Resource.Success(RetrofitClient.webService.getTragosSinAlcohol(non_alcoholic).drinkList)
    }

    suspend fun fetchTragosByNameList(nameDrink : String): Resource<List<Drink>> {
        return Resource.Success(RetrofitClient.webService.getTragosByName(nameDrink).drinkList)
    }

    suspend fun getDrinkFavoritsList(): Resource<List<DrinkEntity>> {
        return Resource.Success(appDatabase.tragoDao().getFavoritsDrink(true))
    }

    suspend fun insertTrago(trago: DrinkEntity) {
        appDatabase.tragoDao().insertTrago(trago)
    }

    suspend fun getDrinkById(id: Int): Resource<DrinkEntity> {
        return Resource.Success(appDatabase.tragoDao().getDrinkById(id))
    }

    suspend fun deleteTrago(trago: DrinkEntity) {
        appDatabase.tragoDao().delete(trago)
    }
}