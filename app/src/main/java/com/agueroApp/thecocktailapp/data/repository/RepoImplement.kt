package com.agueroApp.thecocktailapp.data.repository

import com.agueroApp.thecocktailapp.data.data.TragosDataSource
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.agueroApp.thecocktailapp.domain.repository.Repository
import com.agueroApp.thecocktailapp.vo.Resource

class RepoImplement(private val dataSource: TragosDataSource) : Repository {

    override suspend fun getDrinkListByIngredient(drinkIngredient: String): Resource<List<Drink>> {
        return dataSource.fetchTragosIngredientList(drinkIngredient)
    }

    override suspend fun getDrinkAlcoholicList(alcoholic: String): Resource<List<Drink>> {
        return dataSource.fetchTragosAlcoholicList(alcoholic)
    }

    override suspend fun getDrinkNonAlcoholicList(nonAlcoholic: String): Resource<List<Drink>> {
        return dataSource.fetchTragosNonAlcoholicList(nonAlcoholic)
    }

    override suspend fun getDrinkByNameList(drinkName: String): Resource<List<Drink>> {
        return dataSource.fetchTragosByNameList(drinkName)
    }

    override suspend fun getDrinkFavoritsList(): Resource<List<DrinkEntity>> {
        return dataSource.getDrinkFavoritsList()
    }

    override suspend fun insertTrago(trago: DrinkEntity) {
        dataSource.insertTrago(trago)
    }

    override suspend fun getDrinkId(id: Int): Resource<DrinkEntity> {
        return dataSource.getDrinkById(id)
    }

    override suspend fun deleteTrago(trago: DrinkEntity) {
        dataSource.deleteTrago(trago)
    }
}