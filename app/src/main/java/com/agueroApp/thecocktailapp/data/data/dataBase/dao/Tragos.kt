package com.agueroApp.thecocktailapp.data.data.dataBase.dao

import androidx.room.*
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity

@Dao
interface Tragos {

    @Query("SELECT * FROM DRINKDB")
    suspend fun getAllDrinks(): List<DrinkEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTrago(trago:DrinkEntity)

    @Delete
    suspend fun delete(drink: DrinkEntity)

    @Query("SELECT * FROM DRINKDB WHERE tragoId = :id LIMIT 1")
    suspend fun getDrinkById(id: Int) : DrinkEntity

    @Query("SELECT * FROM DRINKDB WHERE favorite = :favorite")
    suspend fun getFavoritsDrink(favorite: Boolean) : List<DrinkEntity>
}