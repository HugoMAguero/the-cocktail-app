package com.agueroApp.thecocktailapp.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "DRINKDB")
data class DrinkEntity(
    @PrimaryKey
    val tragoId: String,
    @ColumnInfo(name = "trago_imagen")
    val imagen: String?,
    @ColumnInfo(name = "trago_nombre")
    val nombre: String?,
    @ColumnInfo(name = "trago_descripcion")
    val descripcion: String?,
    @ColumnInfo(name = "trago_has_alcohol")
    val hasAlcohol:String?,

    val favorite: Boolean
)