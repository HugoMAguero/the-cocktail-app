package com.agueroApp.thecocktailapp.data

abstract class BaseMapper<I, O> {
    abstract fun map(input: I): O

    fun mapAll(input: Collection<I>?): List<O> {
        val list = ArrayList<O>()
        input?.takeIf { it.isNotEmpty() }?.forEach {
            if (it != null) {
                val mapped = map(it)

                if (!list.contains(mapped)) {
                    list.add(mapped)
                }
            }
        }

        return list
    }
}