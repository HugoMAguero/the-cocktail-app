package com.agueroApp.thecocktailapp.ui.favoritsDrink

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.repository.Repository
import com.agueroApp.thecocktailapp.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class FavoritViewModel(private val repo: Repository): ViewModel() {

    fun fetchDrinkFavorits() = liveData(Dispatchers.IO) {
        emit(Resource.Loading)
        try {
            emit(repo.getDrinkFavoritsList())
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }

    }
}