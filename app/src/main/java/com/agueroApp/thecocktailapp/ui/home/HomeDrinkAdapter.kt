package com.agueroApp.thecocktailapp.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agueroApp.thecocktailapp.R
import com.agueroApp.thecocktailapp.base.BaseViewHolder
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.rv_item_drink.view.*

class HomeDrinkAdapter(private val context: Context, private val listaTragos: List<Drink>, private val itemClick: OnTragoClick) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnTragoClick {
        fun onClick(trago: Drink)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return DrinkViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_item_drink, parent, false))
    }

    override fun getItemCount(): Int {
        return listaTragos.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder) {
            is DrinkViewHolder -> {
                holder.bind(listaTragos[position], position)
            }
        }
    }

    inner class DrinkViewHolder(itemView: View) : BaseViewHolder<Drink>(itemView){
        override fun bind(item: Drink, position: Int) {
            itemView.title_drink.text = item.nombreTrago
            Glide.with(context)
                .load(item.imagenTrago)
                .circleCrop()
                .into(itemView.imageViewDrink)
            itemView.setOnClickListener {
                itemClick.onClick(item)
            }
        }
    }
}