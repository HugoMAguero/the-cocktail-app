package com.agueroApp.thecocktailapp.ui.detailDrinks

import androidx.lifecycle.*
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.repository.Repository
import com.agueroApp.thecocktailapp.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class DetailDrinkViewModel(private val repo: Repository): ViewModel() {

    private val ingredientData = MutableLiveData<String>()
    private val nameData = MutableLiveData<String>()

    fun setIngredient(ingredient : String){
        ingredientData.value = ingredient
    }
    fun setName( nameDrink : String){
        nameData.value = nameDrink
    }

    init {
        setIngredient("vodka")
        setName("Margarita")
    }

    fun insertTragoIntoFavorites(trago: DrinkEntity){
        viewModelScope.launch {
            repo.insertTrago(trago)
        }
    }

    fun deleteTragoFavorites(trago: DrinkEntity){
        viewModelScope.launch {
            repo.deleteTrago(trago)
        }
    }


    fun fetchDrinkById(id : Int ) = liveData(Dispatchers.IO) {
        emit(Resource.Loading)
        try {
            emit(repo.getDrinkId(id))
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }

    }
}