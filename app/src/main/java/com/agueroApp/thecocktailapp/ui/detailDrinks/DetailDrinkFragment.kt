package com.agueroApp.thecocktailapp.ui.detailDrinks

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.agueroApp.thecocktailapp.R
import com.agueroApp.thecocktailapp.data.data.TragosDataSource
import com.agueroApp.thecocktailapp.data.data.dataBase.AppDatabase
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.data.repository.RepoImplement
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.agueroApp.thecocktailapp.viewModel.VMFactory
import com.agueroApp.thecocktailapp.vo.Resource
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_detail_drink.*
import kotlinx.android.synthetic.main.fragment_detail_drink.imageViewDrink
import kotlinx.android.synthetic.main.fragment_detail_drink.progressBarDetail
import kotlinx.android.synthetic.main.fragment_favorit_drink.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.rv_item_drink.*
import kotlinx.android.synthetic.main.rv_item_drink.view.*

class DetailDrinkFragment : Fragment() {

    private val viewModel by viewModels<DetailDrinkViewModel> { VMFactory(
        RepoImplement(
            TragosDataSource(AppDatabase.getDatabase(requireActivity().applicationContext))
        )
    ) }
    private lateinit var drink: Drink

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            drink = it.getParcelable("drink")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_drink, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBarDetail.visibility = View.VISIBLE
        btn_unfavorits_details.setOnClickListener {
            btn_unfavorits_details.visibility = View.GONE
            btn_favorits_details.visibility = View.VISIBLE
            viewModel.insertTragoIntoFavorites(DrinkEntity(drink.idTrago, drink.imagenTrago, drink.nombreTrago, drink.tragoInstruccion, drink.alcoholic, true))
        }

        btn_favorits_details.setOnClickListener {
            btn_unfavorits_details.visibility = View.VISIBLE
            btn_favorits_details.visibility = View.GONE
            viewModel.deleteTragoFavorites(DrinkEntity(drink.idTrago, drink.imagenTrago, drink.nombreTrago, drink.tragoInstruccion, drink.alcoholic, false))
        }

        viewModel.fetchDrinkById(drink.idTrago.toInt()).observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    progressBarDetail.visibility = View.VISIBLE
                    title_detail.visibility = View.GONE
                    title_instructions.visibility = View.GONE
                    imageViewDrink.visibility = View.GONE
                    text_instructions_detail.visibility = View.GONE
                    btn_unfavorits_details.visibility = View.GONE
                }
                is Resource.Success -> {
                    if (result.data != null) {
                        title_detail.text = result.data.nombre
                        Glide.with(requireContext()).load(result.data.imagen).circleCrop().into(view.imageViewDrink)
                        text_instructions_detail.text = result.data.descripcion
                        progressBarDetail.visibility = View.GONE
                        title_detail.visibility = View.VISIBLE
                        title_instructions.visibility = View.VISIBLE
                        imageViewDrink.visibility = View.VISIBLE
                        text_instructions_detail.visibility = View.VISIBLE
                        setDrink()
                    }else {
                        progressBarDetail.visibility = View.GONE
                        title_detail.visibility = View.VISIBLE
                        title_instructions.visibility = View.VISIBLE
                        imageViewDrink.visibility = View.VISIBLE
                        text_instructions_detail.visibility = View.VISIBLE
                        title_detail.text = drink.nombreTrago
                        text_instructions_detail.text = drink.tragoInstruccion
                        Glide.with(requireContext()).load(drink.imagenTrago).circleCrop().into(view.imageViewDrink)
                        setDrink()
                    }
                }
                is Resource.Failure -> {
                    progressBarDetail.visibility = View.GONE
                }
            }
        })

        go_favs.setOnClickListener {
            findNavController().navigate(R.id.action_detailDrinkFragment_to_favoritDrinkFragment2)
        }
    }

    private fun setDrink() {
        if (drink.favorite) {
            btn_favorits_details.visibility = View.VISIBLE
            btn_unfavorits_details.visibility = View.GONE
        }else {
            btn_unfavorits_details.visibility = View.VISIBLE
            btn_favorits_details.visibility = View.GONE
        }
    }
}