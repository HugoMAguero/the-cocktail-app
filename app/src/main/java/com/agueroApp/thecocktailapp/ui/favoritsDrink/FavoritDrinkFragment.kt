package com.agueroApp.thecocktailapp.ui.favoritsDrink

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.agueroApp.thecocktailapp.R
import com.agueroApp.thecocktailapp.data.data.TragosDataSource
import com.agueroApp.thecocktailapp.data.data.dataBase.AppDatabase
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.data.repository.RepoImplement
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.agueroApp.thecocktailapp.viewModel.VMFactory
import com.agueroApp.thecocktailapp.vo.Resource
import kotlinx.android.synthetic.main.fragment_favorit_drink.*

class FavoritDrinkFragment : Fragment() {

    private val viewModel by viewModels<FavoritViewModel> { VMFactory(
        RepoImplement(
            TragosDataSource(AppDatabase.getDatabase(requireActivity().applicationContext))
        )
    ) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorit_drink, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        viewModel.fetchDrinkFavorits().observe(viewLifecycleOwner, Observer {result ->
            when (result) {
                is Resource.Loading -> {
                    progressBarFavorits.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    progressBarFavorits.visibility = View.GONE
                    rv_favorits_drinks.adapter = FavoritDrinkAdapter(requireContext(), result.data)
                }
                is Resource.Failure -> {
                    progressBarFavorits.visibility = View.GONE
                }
            }
        })

        btn_home_favs.setOnClickListener {
            findNavController().navigate(R.id.action_favoritDrinkFragment_to_homeFragment)
        }
    }

    private fun setupRecyclerView() {
        rv_favorits_drinks.layoutManager = LinearLayoutManager(requireContext())
        rv_favorits_drinks.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
    }
}