package com.agueroApp.thecocktailapp.ui.favoritsDrink

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agueroApp.thecocktailapp.R
import com.agueroApp.thecocktailapp.base.BaseViewHolder
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.rv_item_drink.view.*
import kotlinx.android.synthetic.main.rv_item_drink.view.title_drink
import kotlinx.android.synthetic.main.rv_item_favs_drink.view.*

class FavoritDrinkAdapter(private val context: Context, private val listaTragos: List<DrinkEntity>) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return DrinkViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_item_favs_drink, parent, false))
    }

    override fun getItemCount(): Int {
        return listaTragos.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder) {
            is DrinkViewHolder -> {
                holder.bind(listaTragos[position], position)
            }
        }
    }

    inner class DrinkViewHolder(itemView: View) : BaseViewHolder<DrinkEntity>(itemView){
        override fun bind(item: DrinkEntity, position: Int) {
            itemView.nameDrinkFavs.text = item.nombre
            itemView.detailDrinkFavs.text = item.descripcion
            Glide.with(context).load(item.imagen).circleCrop().into(itemView.imageFavsDrink)
        }
    }
}