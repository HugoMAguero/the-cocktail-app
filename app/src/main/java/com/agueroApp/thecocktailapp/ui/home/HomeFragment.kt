package com.agueroApp.thecocktailapp.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.agueroApp.thecocktailapp.R
import com.agueroApp.thecocktailapp.data.data.TragosDataSource
import com.agueroApp.thecocktailapp.data.data.dataBase.AppDatabase
import com.agueroApp.thecocktailapp.data.repository.RepoImplement
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.agueroApp.thecocktailapp.viewModel.VMFactory
import com.agueroApp.thecocktailapp.vo.Resource
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(), HomeDrinkAdapter.OnTragoClick {

    private val viewModel by viewModels<HomeViewModel> { VMFactory(
        RepoImplement(
            TragosDataSource(AppDatabase.getDatabase(requireActivity().applicationContext))
        )
    ) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupSearchNameView()
        setupSearchIngredientView()
        viewModel.fetchDrinkByName.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    rv_drinks.adapter = HomeDrinkAdapter(requireContext(), result.data, this)
                }
                is Resource.Failure -> {
                    progressBar.visibility = View.GONE
                }
            }
        })

        viewModel.fetchDrinkByIngredient.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    rv_drinks.adapter = HomeDrinkAdapter(requireContext(), result.data, this)
                }
                is Resource.Failure -> {
                    progressBar.visibility = View.GONE
                }
            }
        })

        btn_ir_favoritos_home.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_favoritDrinkFragment)
        }

        btn_alcoholic.setOnClickListener {
            viewModel.fetchDrinkByAlcoholic().observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        progressBar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        progressBar.visibility = View.GONE
                        rv_drinks.adapter = HomeDrinkAdapter(requireContext(), result.data, this)
                    }
                    is Resource.Failure -> {
                        progressBar.visibility = View.GONE
                    }
                }
            })
        }

        btn_non_alcoholic.setOnClickListener {
            viewModel.fetchDrinkByNonAlcoholic().observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        progressBar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        progressBar.visibility = View.GONE
                        rv_drinks.adapter = HomeDrinkAdapter(requireContext(), result.data, this)
                    }
                    is Resource.Failure -> {
                        progressBar.visibility = View.GONE
                    }
                }
            })
        }
    }

    private fun setupRecyclerView() {
        rv_drinks.layoutManager = LinearLayoutManager(requireContext())
        rv_drinks.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
    }

    private fun setupSearchNameView() {
        searchByName.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.setName(query!!)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

        })
    }

    private fun setupSearchIngredientView() {
        searchByName.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.setIngredient(query!!)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

        })
    }

    override fun onClick(drink: Drink) {
        val bundle = Bundle()
        bundle.putParcelable("drink", drink)
        findNavController().navigate(R.id.action_homeFragment_to_detailDrinkFragment, bundle)
    }
}
