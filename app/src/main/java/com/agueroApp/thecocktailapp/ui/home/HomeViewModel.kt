package com.agueroApp.thecocktailapp.ui.home


import androidx.lifecycle.*
import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.repository.Repository
import com.agueroApp.thecocktailapp.vo.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class HomeViewModel(private val repo: Repository): ViewModel() {

    private val ingredientData = MutableLiveData<String>()
    private val nameData = MutableLiveData<String>()

    fun setIngredient(ingredient: String) {
        ingredientData.value = ingredient
    }

    fun setName(nameDrink: String) {
        nameData.value = nameDrink
    }

    init {
        setIngredient("vodka")
        setName("Margarita")
    }

    val fetchDrinkByName = nameData.distinctUntilChanged().switchMap { nameDrink ->
        liveData(Dispatchers.IO) {
            emit(Resource.Loading)
            try {
                emit(repo.getDrinkByNameList(nameDrink))
            }catch (e: Exception){
                emit(Resource.Failure(e))
            }
        }
    }

    val fetchDrinkByIngredient = ingredientData.distinctUntilChanged().switchMap { ingrediente ->
        liveData(Dispatchers.IO) {
            emit(Resource.Loading)
            try {
                emit(repo.getDrinkListByIngredient(ingrediente))
            }catch (e: Exception){
                emit(Resource.Failure(e))
            }
        }
    }

    fun fetchDrinkByAlcoholic() = liveData(Dispatchers.IO) {
        emit(Resource.Loading)
        try {
            emit(repo.getDrinkAlcoholicList("Alcoholic"))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun fetchDrinkByNonAlcoholic() = liveData(Dispatchers.IO) {
        emit(Resource.Loading)
        try {
            emit(repo.getDrinkNonAlcoholicList("Non_Alcoholic"))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }
}