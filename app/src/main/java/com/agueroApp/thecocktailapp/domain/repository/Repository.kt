package com.agueroApp.thecocktailapp.domain.repository

import com.agueroApp.thecocktailapp.data.entity.DrinkEntity
import com.agueroApp.thecocktailapp.domain.model.Drink
import com.agueroApp.thecocktailapp.vo.Resource

interface Repository {

    suspend fun getDrinkListByIngredient(ingredients : String) : Resource<List<Drink>>

    suspend fun getDrinkAlcoholicList(ingredients : String) : Resource<List<Drink>>

    suspend fun getDrinkNonAlcoholicList(ingredients : String) : Resource<List<Drink>>

    suspend fun getDrinkByNameList(ingredients : String) : Resource<List<Drink>>

    suspend fun getDrinkFavoritsList() : Resource<List<DrinkEntity>>

    suspend fun insertTrago(trago: DrinkEntity)

    suspend fun getDrinkId(id: Int) : Resource<DrinkEntity>

    suspend fun deleteTrago(trago: DrinkEntity)
}