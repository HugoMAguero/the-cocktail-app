package com.agueroApp.thecocktailapp.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.SerializedName

@Parcelize
data class Drink(
    @SerializedName("idDrink")
    var idTrago: String = "",
    @SerializedName("strDrink")
    var nombreTrago: String = "",
    @SerializedName("strInstructions")
    var tragoInstruccion: String = "",
    @SerializedName("strDrinkThumb")
    var imagenTrago: String = "",
    @SerializedName("strAlcoholic")
    var alcoholic: String = "",
    var favorite: Boolean = false
) : Parcelable

data class DrinkList(
    @SerializedName("drinks")
    val drinkList: List<Drink> = listOf()
)