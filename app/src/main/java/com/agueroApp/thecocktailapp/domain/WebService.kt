package com.agueroApp.thecocktailapp.domain

import com.agueroApp.thecocktailapp.domain.model.DrinkList
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {

    @GET("filter.php")
    suspend fun getTragosByName(@Query("s") nameTrago : String ) : DrinkList

    @GET("filter.php")
    suspend fun getTragosByIngredients(@Query("i") ingredients : String) : DrinkList

    @GET("filter.php")
    suspend fun getTragosConAlcohol(@Query("a") Alcoholic : String ) : DrinkList

    @GET("filter.php")
    suspend fun getTragosSinAlcohol(@Query("a") Non_Alcoholic : String ) : DrinkList

    @GET("lookup.php")
    suspend fun getTragosById(@Query("i") id : String ) : DrinkList

}